
This is a Transport Application part of the coding challenge from 2pointb. It's an automation of a real life transportation network. At present the app supports

	1. Pickup/drop off a customer
	2. 2 modes of service 
		a. Sedan
		b. RaceCar
	3. Ability to browse a website of choice.

When the application starts the user can select the mode of service. As stated above the app at present offers 2 modes of Service (a) Sedan or (b) RaceCar. The user is prompted to choose any one of the options. Based on the option choosen a Car is dispatched to the Customer. When the car is dispatched to the Customer the user can see at what co-ordinates the Car is moving and once picked up at what co-ordinates the Car is progressing in-order to reach the destination. The user can also browse a website of their choice.

Below are the list of features and changes to be done in priority:

	1. There's no unit test case project present. I would be adding them immediately in the next iteration.
	2. Buildings are not yet a part of the city. 
		Note: I feel this is the most challenging part of the program. I'm likely going to choose a graph based traversal and include buildings in my City[height,width]. The present design has the car traversal logic in the Move() method (Car class), changing this method to traverse the graph will be suffice.
	3. Generate a map out of the path followed by the Car from its inception to dropping the passenger off at their desired destination. Color coding will differentiate between path travelled prior to pickup and after.
	4. Web API
Bugs:
So far no bugs have been identified but again am sure as I go an adding the test cases all the bugs found would be added in the issue section and will be addressed based on the severity.