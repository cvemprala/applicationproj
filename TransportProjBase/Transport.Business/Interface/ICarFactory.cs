﻿using Transport.Business.Abstract;

namespace Transport.Business.Interface {
   public interface ICarFactory {
      Car CreateCar(string choice);
   }
}
