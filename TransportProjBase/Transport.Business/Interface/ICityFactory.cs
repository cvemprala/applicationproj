﻿using Transport.Business.Implementation;

namespace Transport.Business.Interface {
   public interface ICityFactory {
      City CurrentCity { get; }
   }
}
