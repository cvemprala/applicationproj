﻿using System;
using System.Configuration;
using Transport.Business.Abstract;
using Transport.Business.Common;
using Transport.Business.Implementation;
using Transport.Business.Interface;

namespace Transport.Business.Factory {
   public class CarFactory : ICarFactory {
      private int _cityHeight;
      private int _cityWidth;
      private City _city;

      public CarFactory(City city) {
         _cityWidth = 10;
         _cityHeight = 10;
         _city = city;

         if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CityWidth"]) ||
             string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CityHeight"])) {
            _cityWidth = 10;
            _cityHeight = 10;
         } else {
            Int32.TryParse(ConfigurationManager.AppSettings["CityWidth"], out _cityWidth);
            Int32.TryParse(ConfigurationManager.AppSettings["CityHeight"], out _cityHeight);
         }
      }

      public Car CreateCar(string choice) {
         switch (choice) {
            case "s":
               return GetSedan();
            case "r":
               return GetRaceCar();
            default:
               throw new ArgumentException(string.Format("The choice: [{0}] is not supported by the application. ", choice));
         }
      }

      private Sedan GetSedan() {
         var sedan = new Sedan(RandomProvider.Next(0, _cityWidth), RandomProvider.Next(0, _cityHeight), _city, null);
         var passenger = new Passenger(RandomProvider.Next(0, _cityWidth), RandomProvider.Next(0, _cityHeight), RandomProvider.Next(0, _cityWidth), RandomProvider.Next(0, _cityHeight), _city);
         sedan.PickupPassenger(passenger);
         return sedan;
      }

      private RaceCar GetRaceCar() {
         var raceCar = new RaceCar(RandomProvider.Next(0, _cityWidth), RandomProvider.Next(0, _cityHeight), _city, null);
         var passenger = new Passenger(RandomProvider.Next(0, _cityWidth), RandomProvider.Next(0, _cityHeight), RandomProvider.Next(0, _cityWidth), RandomProvider.Next(0, _cityHeight), _city);
         raceCar.PickupPassenger(passenger);
         return raceCar;
      }
   }
}
