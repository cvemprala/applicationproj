﻿using System;
using System.Configuration;
using Transport.Business.Implementation;
using Transport.Business.Interface;

namespace Transport.Business.Factory {
   public class CityFactory : ICityFactory {
      private int _cityHeight;
      private int _cityWidth;

      private static Random _rnd = new Random();
      private static object _sync = new object();

      public City CurrentCity { get; private set; }

      public CityFactory() {
         _cityWidth = 10;
         _cityHeight = 10;

         if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CityWidth"]) ||
             string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CityHeight"])) {
            _cityWidth = 10;
            _cityHeight = 10;
         } else {
            Int32.TryParse(ConfigurationManager.AppSettings["CityWidth"], out _cityWidth);
            Int32.TryParse(ConfigurationManager.AppSettings["CityHeight"], out _cityHeight);
         }
         CurrentCity = new City(_cityWidth, _cityHeight);
      }
   }
}
