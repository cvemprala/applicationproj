﻿using Transport.Business.Abstract;
using Transport.Business.Implementation;

namespace Transport.Business.Data {
   public class CityPosition {
      public Car Car { get; set; }
      public Passenger Passenger { get; set; }
   }
}
