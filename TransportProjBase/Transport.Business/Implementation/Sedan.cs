﻿using System;
using System.Configuration;
using System.IO;
using Transport.Business.Abstract;
using Transport.Business.Common;

namespace Transport.Business.Implementation {
   public class Sedan : Car {

      /// <summary>
      /// Every car can move for 'n' paces at a time. MobilityIndex is used for tracking and implementing this.
      /// </summary>
      private int _mobilityIndex;
      public int MobilityIndex { get; private set; }

      public Sedan(int xPos, int yPos, City city, Passenger passenger)
         : base(xPos, yPos, city, passenger) {
         if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["SedanMobilityIndex"])) {
            _mobilityIndex = 1;
         } else {
            Int32.TryParse(ConfigurationManager.AppSettings["SedanMobilityIndex"], out _mobilityIndex);
         }
         MobilityIndex = _mobilityIndex;
      }

      internal override void PickUp() {
         Console.WriteLine("\nThe passenger is picked up.\n");
         WritePositionToConsole();
      }

      internal override void MoveUp() {
         YPos++;
         Console.WriteLine(string.Format("Sedan moving in {0} direction", DirectionType.Up.ToString()));
         WritePositionToConsole();
      }

      internal override void MoveDown() {
         YPos--;
         Console.WriteLine(string.Format("Sedan moving in {0} direction", DirectionType.Down.ToString()));
         WritePositionToConsole();
      }

      internal override void MoveRight() {
         XPos++;
         Console.WriteLine(string.Format("Sedan moving in {0} direction", DirectionType.Right.ToString()));
         WritePositionToConsole();
      }

      internal override void MoveLeft() {
         XPos--;
         Console.WriteLine(string.Format("Sedan moving in {0} direction", DirectionType.Left.ToString()));
         WritePositionToConsole();
      }

      protected override void WritePositionToConsole() {
         Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
      }
   }
}
