﻿using System;
using System.Configuration;
using Transport.Business.Abstract;
using Transport.Business.Common;

namespace Transport.Business.Implementation {
   public class RaceCar : Car {
      /// <summary>
      /// Every car can move for 'n' paces at a time. MobilityIndex is used for tracking and implementing this.
      /// </summary>
      private int _mobilityIndex;
      public int MobilityIndex { get; private set; }

      public RaceCar(int xPos, int yPos, City city, Passenger passenger)
         : base(xPos, yPos, city, passenger) {
         if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["RaceCarMobilityIndex"])) {
            _mobilityIndex = 2;
         } else {
            Int32.TryParse(ConfigurationManager.AppSettings["RaceCarMobilityIndex"], out _mobilityIndex);
         }
         MobilityIndex = _mobilityIndex;
      }

      internal override void PickUp() {
         Console.WriteLine("\nThe passenger is picked up.\n");
         WritePositionToConsole();
      }

      internal override void MoveUp() {
         YPos++;
         Console.WriteLine(string.Format("RaceCar moving in {0} direction", DirectionType.Up.ToString()));
         WritePositionToConsole();
      }

      internal override void MoveDown() {
         YPos--;
         Console.WriteLine(string.Format("RaceCar moving in {0} direction", DirectionType.Down.ToString()));
         WritePositionToConsole();
      }

      internal override void MoveRight() {
         XPos++;
         Console.WriteLine(string.Format("RaceCar moving in {0} direction", DirectionType.Right.ToString()));
         WritePositionToConsole();
      }

      internal override void MoveLeft() {
         XPos--;
         Console.WriteLine(string.Format("RaceCar moving in {0} direction", DirectionType.Left.ToString()));
         WritePositionToConsole();
      }

      protected override void WritePositionToConsole() {
         Console.WriteLine(String.Format("RaceCar moved to x - {0} y - {1}", XPos, YPos));
      }
   }
}
