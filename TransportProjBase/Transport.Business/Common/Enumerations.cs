﻿
namespace Transport.Business.Common {
   public enum DirectionType {
      Left = 1,
      Right,
      Up,
      Down,
      Arrived,
      PickUp,
      Unknown
   }
}
