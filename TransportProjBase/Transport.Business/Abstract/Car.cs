﻿using System;
using System.IO;
using Transport.Business.Common;
using Transport.Business.Implementation;

namespace Transport.Business.Abstract {
   public abstract class Car {
      public int XPos { get; protected set; }
      public int YPos { get; protected set; }
      public Passenger Passenger { get; private set; }
      public City City { get; private set; }

      protected Car(int xPos, int yPos, City city, Passenger passenger) {
         XPos = xPos;
         YPos = yPos;
         City = city;
         Passenger = passenger;
      }

      protected virtual void WritePositionToConsole() {
         Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
      }

      public void PickupPassenger(Passenger passenger) {
         Passenger = passenger;
      }

      /// <summary>
      /// Based on the mobilityIndex the Run method decides on how many spaces to move. This is agnostic 
      /// to the model of the car. So there by tomorrow if I get a different automobile passing the
      /// mobilityIndex attribute is suffice.
      /// </summary>
      /// <param name="mobilityIndex"></param>
      /// <returns></returns>
      public DirectionType Run(int mobilityIndex) {
         int i = 0;
         DirectionType directionToMove = DirectionType.Unknown;
         while (i < mobilityIndex) {
            directionToMove = GetDirection();
            if (directionToMove.ToString().Equals(DirectionType.Unknown.ToString(), StringComparison.InvariantCultureIgnoreCase)) {
               throw new InvalidDataException("Something wrong happened cannot find where the car is. Please file an Issue will fix it ASAP. Apologies for the inconvenience.");
            }
            //Quits the loop, 
            //    1. when the passenger has reached the destination.
            //    2. when the car has arrived to pick up the passenger in order start downloading the website
            if (directionToMove.ToString().Equals(DirectionType.Arrived.ToString(), StringComparison.InvariantCultureIgnoreCase) ||
               directionToMove.ToString().Equals(DirectionType.PickUp.ToString(), StringComparison.InvariantCultureIgnoreCase)) {
               break;
            }
            i++;
         }
         return directionToMove;
      }

      /// <summary>
      /// The business logic should define in which direction to move. Irrespective of the type of the car, taking directions
      /// is a common operation. This method helps in deciding the direction for the car to move.
      /// The possible directions can be,
      ///   DirectionType.Up
      ///   DirectionType.Down
      ///   DirectionType.Left
      ///   DirectionType.Right
      ///   DirectionType.PickUp  --> When the passenger is picked up.
      ///   DirectionType.Arrived --> Reached destination.   
      ///   DirectionType.Unknown --> When Direction is Unknown an exception is thrown.
      /// </summary>
      /// <returns></returns>
      DirectionType Move() {
         DirectionType directionType;
         if (Passenger.Car == null) {
            if (YPos < Passenger.StartingYPos && YPos < City.YMax) {
               directionType = DirectionType.Up;
            } else if (YPos > Passenger.StartingYPos && YPos > 0) {
               directionType = DirectionType.Down;
            } else if (XPos < Passenger.StartingXPos && XPos < City.XMax) {
               directionType = DirectionType.Right;
            } else if (XPos > Passenger.StartingXPos && XPos > 0) {
               directionType = DirectionType.Left;
            } else if (XPos == Passenger.StartingXPos && YPos == Passenger.StartingYPos) {
               directionType = DirectionType.PickUp;
            } else {
               directionType = DirectionType.Unknown;
            }
         } else {
            if (YPos < Passenger.DestinationYPos && YPos < City.YMax) {
               directionType = DirectionType.Up;
            } else if (YPos > Passenger.DestinationYPos && YPos > 0) {
               directionType = DirectionType.Down;
            } else if (XPos < Passenger.DestinationXPos && XPos < City.XMax) {
               directionType = DirectionType.Right;
            } else if (XPos > Passenger.DestinationXPos && XPos > 0) {
               directionType = DirectionType.Left;
            } else if (XPos == Passenger.DestinationXPos && YPos == Passenger.DestinationYPos) {
               directionType = DirectionType.Arrived;
            } else {
               directionType = DirectionType.Unknown;
            }
         }

         return directionType;
      }

      /// <summary>
      /// Based on the direction of the vehicle movement update the co-ordinates.
      /// </summary>
      /// <returns></returns>
      DirectionType GetDirection() {
         DirectionType directionToMove = this.Move();
         switch (directionToMove) {
            case DirectionType.Up:
               MoveUp();
               break;
            case DirectionType.Down:
               MoveDown();
               break;
            case DirectionType.Left:
               MoveLeft();
               break;
            case DirectionType.Right:
               MoveRight();
               break;
            case DirectionType.PickUp:
               PickUp();
               break;
         }
         return directionToMove;
      }

      internal abstract void MoveUp();

      internal abstract void MoveDown();

      internal abstract void MoveRight();

      internal abstract void MoveLeft();

      internal abstract void PickUp();
   }
}
