﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Transport.Business.Abstract;
using Transport.Business.Common;
using Transport.Business.Factory;
using Transport.Business.Implementation;

namespace TransportProj {
   class Program {
      private static string _websiteToVisit = ConfigurationManager.AppSettings["Website"] ?? "http://2pointb.com/";
      static void Main(string[] args) {
         Console.Write("For transportation, please select\n\t'S' for Sedan\n\t'R' for Race Car.\nIf you want us to choose for you please press anyother character.\n");

         int ascii = Console.Read();

         string choice = Convert.ToChar(ascii).ToString();

         if (!"s".Equals(choice, StringComparison.InvariantCultureIgnoreCase) && !"r".Equals(choice, StringComparison.InvariantCultureIgnoreCase)) {
            int i = RandomProvider.Next(1, 100);
            choice = i % 2 == 0 ? "s" : "r";
         }


         if ("s".Equals(choice, StringComparison.InvariantCultureIgnoreCase)) {
            Console.WriteLine("A Sedan will be dispatched at your service immediately.\n");
         } else if ("r".Equals(choice, StringComparison.InvariantCultureIgnoreCase)) {
            Console.WriteLine("A Race car will be at your service in t-5 seconds and counting.\n");
         }


         var cityFactory = new CityFactory();
         var carFactory = new CarFactory(cityFactory.CurrentCity);

         Car carObject = carFactory.CreateCar(choice);
         if (carObject == null) {
            Console.WriteLine("Could not create a Car Object. Please create an issue request will check into it ASAP. Apologies for the inconvenience.\nPress any key to close the screen.");
            Console.ReadKey();
            return;
         }

         Passenger passenger = carObject.Passenger;
         if (passenger == null) {
            Console.WriteLine("Could not create a Passenger Object. Please create an issue request will check into it ASAP. Apologies for the inconvenience.\nPress any key to close the screen.");
            Console.ReadKey();
            return;
         }

         System.Console.WriteLine(string.Format("Current car XPos:[{0}], YPos:[{1}]", carObject.XPos, carObject.YPos));
         System.Console.WriteLine(string.Format("Current Passenger XPos:[{0}], YPos:[{1}]", carObject.Passenger.StartingXPos, carObject.Passenger.StartingYPos));
         System.Console.WriteLine(string.Format("Passenger destination XPos:[{0}], YPos:[{1}]\n", carObject.Passenger.DestinationXPos, carObject.Passenger.DestinationYPos));

         while (!passenger.IsAtDestination()) {
            try {
               Tick(carObject, passenger);
            } catch (InvalidDataException idEx) {
               Console.WriteLine(string.Format("Error: {0}.\nPress any key to close the screen.", idEx.Message));
               break;
            } catch (Exception ex) {
               Console.WriteLine(string.Format("Error: {0}.\nPress any key to close the screen.", ex.Message));
               break;
            }
         }

         if (passenger.IsAtDestination())
            Console.WriteLine("Reached the destination.\nPress any key to close the screen.");

         Console.ReadKey();
      }

      /// <summary>
      /// Takes one action (move the car one spot or pick up the passenger).
      /// </summary>
      /// <param name="car">The car to move</param>
      /// <param name="passenger">The passenger to pick up</param>
      private static void Tick(Car car, Passenger passenger) {
         Console.WriteLine("\n\tTracking total hops.");
         int mobilityIndex = (car is Sedan) ? (car as Sedan).MobilityIndex : (car as RaceCar).MobilityIndex;
         DirectionType direction = car.Run(mobilityIndex);
         if (direction.ToString().Equals(DirectionType.PickUp.ToString())) {
            passenger.Car = car;
         }

         if (passenger.Car != null) {
            GetData();
         }
      }

      static async void GetData() {
         string webData = await Task.Run(() => VisitWebSite());
         if (string.IsNullOrWhiteSpace(webData)) {
            Console.WriteLine(string.Format("Something went wrong while fetching the website data for the website: [{0}]", _websiteToVisit));
         } else {
            Console.WriteLine(string.Format("Data downloaded from website: [{0}].", _websiteToVisit));
         }
      }

      /// <summary>
      /// Downloads the web data and send back the string data
      /// Ref: https://msdn.microsoft.com/en-us/library/456dfw4f(v=vs.110).aspx
      /// </summary>
      /// <returns></returns>
      private static string VisitWebSite() {
         string responseFromServer = string.Empty;
         try {
            WebRequest request = WebRequest.Create(_websiteToVisit);
            // Get the response.
            using (WebResponse response = request.GetResponse()) {
               Stream dataStream = response.GetResponseStream();
               using (var reader = new StreamReader(dataStream)) {
                  responseFromServer = reader.ReadToEnd();
               }
            }
         } catch (Exception ex) {
            Console.WriteLine(string.Format("Error occured while fetching data from [{0}].\nError: {1}", _websiteToVisit, ex.Message));
         }

         return responseFromServer;
      }
   }
}
